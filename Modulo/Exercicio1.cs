
using System;

namespace PucAtpClass
{
    public class Exercicio1
    {
        public void rodar()
        {

            double nota1 = 0;
            double nota2 = 0;
            double nota3 = 0;
            string? typeCalculo = "P";

            Console.WriteLine("Entre com a primeira nota:");
            nota1 = ModuloUtil.checkParse(Console.ReadLine());
            Console.WriteLine("Entre com a segunda nota:");
            nota2 = ModuloUtil.checkParse(Console.ReadLine());
            Console.WriteLine("Entre com a terceira nota:");
            nota3 = ModuloUtil.checkParse(Console.ReadLine());
            Console.WriteLine("Entre com o tipo de calculo:");
            typeCalculo = Console.ReadLine();
            double Sainda = CalcularEntrada(nota1, nota2, nota3, typeCalculo);
            Console.WriteLine("Resultado:{0}", Sainda);

        }

        private double CalcularEntrada(double nota1, double nota2, double nota3, string? TypeCalculo)
        {
            double retorno = 0;
            if (TypeCalculo == "A")
            {
                retorno = (nota1 + nota2 + nota3) / 3;
            }
            else if (TypeCalculo == "P")
            {
                int peso1 = 5;
                int peso2 = 3;
                int peso3 = 2;
                retorno = ((nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);
            }
            else
            {
                Console.WriteLine("Parametro de entrada tipo invalido!");
            }
            return retorno;
        }


    }
}