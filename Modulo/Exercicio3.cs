//namespace PucAtpClass;
using System;
namespace PucAtpClass
{
    public class Exercicio3
    {
        public void rodar()
        {
            Console.WriteLine("Digite n numeros separados por espaço");
            string? entrada = Console.ReadLine();
            if (!string.IsNullOrEmpty(entrada))
            {
                string[] vt = entrada.Split(' ');
                if (vt.Length == 3)
                {
                    ordenar(vt);
                }
            }
        }



        public void ordenar(string[] vt)
        {
            double menor = 0;
            double[] vtOrdenado = new double[vt.Length];
            double aux = 0;
            for (int i = 0; i < vt.Length; i++)
            {
                menor = double.MaxValue;
                for (int y = i; y < vt.Length; y++)
                {
                    aux = ModuloUtil.checkParse(vt[y]);
                    if (aux < menor)
                    {
                        menor = aux;

                    }
                }
                vtOrdenado[i] = menor;
            }
            for (int i = 0; i < vtOrdenado.Length; i++)
            {
                Console.WriteLine(vtOrdenado[i]);
            }

        }
    }
}