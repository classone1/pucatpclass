namespace PucAtpClass
{
    public class ModuloUtil
    {
        public static double checkParse(string? valor)
        {
            double retorno = 0;
            if (!string.IsNullOrWhiteSpace(valor))
            {
                double.TryParse(valor, out retorno);
            }
            return retorno;
        }
    }
}