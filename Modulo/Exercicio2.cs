//namespace PucAtpClass;
using System;
namespace PucAtpClass
{
    public class Exercicio2
    {
        public void rodar()
        {
            double salario = 0;
            int numeroFilhos = 0;
            int contador = 0;
            double acumuladorSalario = 0;
            string? controleParada = "S";

            while (controleParada != null && controleParada.Equals("S"))
            {
                contador++;
                Console.WriteLine("Salario?");
                salario = ModuloUtil.checkParse(Console.ReadLine());
                acumuladorSalario += salario;
                Console.WriteLine("Numero de filhos?");
                numeroFilhos = (int)ModuloUtil.checkParse(Console.ReadLine());
                Console.WriteLine("Media Salario ate o momento: {0}", (acumuladorSalario / contador));
                Console.WriteLine("Adicionar mais registros? S para Continuar!");
                controleParada = Console.ReadLine();
            }
        }

    }
}