﻿using System;
using System.Runtime.CompilerServices;


namespace PucAtpClass
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcaoEscolhida = 0;
            Console.WriteLine("Escolha o exercicio abaixo:");
            Console.WriteLine("1 para Exerxicio 1");
            Console.WriteLine("2 para Exerxicio 2");
            Console.WriteLine("3 para Exerxicio 3");
            Console.WriteLine("4 para Exerxicio 4");
            opcaoEscolhida = (int)ModuloUtil.checkParse(Console.ReadLine());

            //switch substituin emcasos de muitas opções usar (if e Else) melhorando a legiblidade do codigo

            switch (opcaoEscolhida)
            {
                case 1:
                    {
                        new Exercicio1().rodar();
                        break;
                    }
                case 2:
                    {
                        new Exercicio2().rodar();
                        break;
                    }
                case 3:
                    {
                        new Exercicio3().rodar();
                        break;
                    }
                case 4:
                    {
                        new Exercicio4().rodar();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Opção invalida");
                        break;
                    }
            }

            Console.WriteLine("Fim");
            Console.ReadLine();


        }
    }
}
