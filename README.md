## Projeto:
- Exercicio ATP  Puc

## Environment:
- .NET version: 7.0


## IDE:
- Visual Code

## Git:
```sh
Git Clone https://gitlab.com/classone1/pucatpclass.git
```
## Run the application

```sh
dotnet restore
dotnet run
```


## OBS:
- Projeto com fins didatico
- Por se didatico não foi feito o tratamento da entrada de dados considerando somente o caminho feliz
- Atente-se em não utilizar recursos ainda não passado pelo professor
- Todos exercicios poderiam ser em um unico arquivo mas para organizar e facilitar a manutenção e o entendimento, foi criado um arquivo e uma classe para cada exercici e agrupados na pasta modulos. 